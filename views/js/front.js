/**
 * NOTICE OF LICENSE
 *
 * This file is licensed under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code.
 *
 * @author    Active Design <office@activedesign.ro>
 * @copyright 2016-2018 Active Design
 * @license   LICENSE.txt
 */

document.addEventListener("DOMContentLoaded", function () {
    if (!$('#nutriFacts').length) {
        return;
    }

    $('#nutriFacts').nutritionLabel({
        //default fixedWidth of the nutrition label
        width: 280,

        //to allow custom width - usually needed for mobile sites
        allowCustomWidth: false,
        widthCustom: 'auto',

        //to allow the label to have no border
        allowNoBorder: false,

        //to enable rounding of the nutritional values based on the FDA rounding rules http://goo.gl/RMD2O
        allowFDARounding: false,

        //to enabled the google analytics event logging
        allowGoogleAnalyticsEventLog: false,
        gooleAnalyticsFunctionName: 'ga',

        //enable triggering of user function on quantity change: global function name
        userFunctionNameOnQuantityChange: null,
        //enable triggering of user function on quantity change: handler instance
        userFunctionOnQuantityChange: null,

        //when set to true, this will hide the values if they are not applicable
        hideNotApplicableValues: false,

        //the brand name of the item for this label (eg. just salad)
        brandName: 'Brand where this item belongs to',
        //to scroll the ingredients if the innerheight is > scrollHeightComparison
        scrollLongIngredients: false,
        scrollHeightComparison: 100,
        //the height in px of the ingredients div
        scrollHeightPixel: 95,
        //this is to set how many decimal places will be shown on the nutrition values (calories, fat, protein, vitamin a, iron, etc)
        decimalPlacesForNutrition: 1,
        //this is to set how many decimal places will be shown for the "% daily values*"
        decimalPlacesForDailyValues: 0,
        //this is to set how many decimal places will be shown for the serving unit quantity textbox
        decimalPlacesForQuantityTextbox: 1,

        //to scroll the item name if the jQuery.height() is > scrollLongItemNamePixel
        scrollLongItemName: true,
        scrollLongItemNamePixel: 36,
        scrollLongItemNamePixel2018Override: 34, //this is needed to fix some issues on the 2018 label as the layout of the label is very different than the legacy one

        //show the customizable link at the bottom
        showBottomLink: false,
        //url for the customizable link at the bottom
        urlBottomLink: '',
        //link name for the customizable link at the bottom
        nameBottomLink: '',

        //this value can be changed and the value of the nutritions will be affected directly
        //the computation is "current nutrition value" * "serving unit quantity value" = "final nutrition value"
        //this can't be less than zero, all values less than zero is converted to zero
        //the textbox to change this value is visible / enabled by default
        //if the initial value of the serving size unit quantity is less than or equal to zero, it is converted to 1.0
        //when enabled, user can change this value by clicking the arrow or changing the value on the textbox and pressing enter. the value on the label will be updated automatically
        //different scenarios and the result if this feature is enabled
        //NOTE 1: [ ] => means a textbox will be shown
        //NOTE 2: on all cases below showServingUnitQuantityTextbox == true AND showServingUnitQuantity == true
        //if showServingUnitQuantity == false, the values that should be on the 'serving size div' are empty or null
        //CASE 1a: valueServingSizeUnit != '' (AND NOT null) && valueServingUnitQuantity >= 0
        //RESULT: textServingSize [valueServingUnitQuantity] valueServingSizeUnit

        //NOTE 3: on all cases below showServingUnitQuantityTextbox == true AND showItemName == true
        //if showItemName == false, the values that should be on the 'item name div' are empty or null
        //CASE 1b: valueServingSizeUnit != '' (AND NOT null) && valueServingUnitQuantity <= 0
        //RESULT: [valueServingUnitQuantity default to 1.0] itemName
        //CASE 3a: valueServingSizeUnit == '' (OR null) && valueServingUnitQuantity > 0
        //RESULT: [valueServingUnitQuantity] itemName
        //CASE 3b: valueServingSizeUnit == '' (OR null) && valueServingUnitQuantity <= 0
        //RESULT: [valueServingUnitQuantity default to 1.0] itemName

        //NOTE 4: to see the different resulting labels, check the html/demo-texbox-case*.html files
        valueServingUnitQuantity: nutriFields.size.value,
        valueServingSizeUnit: nutriFields.units.value,
        showServingUnitQuantityTextbox: false,
        //the name of the item for this label (eg. cheese burger or mayonnaise)
        itemName: 'Item / Ingredient Name',
        showServingUnitQuantity: true,
        //allow hiding of the textbox arrows
        hideTextboxArrows: true,

        //these 2 settings are used internally.
        //this is just added here instead of a global variable to prevent a bug when there are multiple instances of the plugin like on the demo pages
        originalServingUnitQuantity: 0,
        //this is used to fix the computation issue on the textbox
        nutritionValueMultiplier: 1,
        //this is used for the computation of the servings per container
        totalContainerQuantity: 1,

        //default calorie intake
        calorieIntake: 2000,

        //these are the recommended daily intake values
        dailyValueTotalFat: 65,
        dailyValueSatFat: 20,
        dailyValueCholesterol: 300,
        dailyValueSodium: 2400,
        dailyValuePotassium: 3500,
        dailyValuePotassium_2018: 4700,
        dailyValueCarb: 300,
        dailyValueFiber: 25,
        dailyValueCalcium: 1300,
        dailyValueIron: 18,
        dailyValueVitaminD: 20,
        dailyValueAddedSugar: 50,

        //these values can be change to hide some nutrition values
        showCalories: true,
        showFatCalories: true,
        showTotalFat: true,
        showSatFat: true,
        showTransFat: true,
        showPolyFat: true,
        showMonoFat: true,
        showCholesterol: true,
        showSodium: true,
        showPotassium: false, //this is for the legacy version, this is the only value that is default to be hidden
        showPotassium_2018: true, //this is for the 2018 version
        showTotalCarb: true,
        showFibers: true,
        showSugars: true,
        showAddedSugars: true,
        showSugarAlcohol: true,
        showProteins: true,
        showVitaminA: true,
        showVitaminC: true,
        showVitaminD: true,
        showCalcium: true,
        showIron: true,

        //to show the 'amount per serving' text
        showAmountPerServing: true,
        //to show the 'servings per container' data and replace the default 'Serving Size' value (without unit and servings per container text and value)
        showServingsPerContainer: true,
        //to show the item name. there are special cases where the item name is replaced with 'servings per container' value
        showItemName: false,
        //show the brand where this item belongs to
        showBrandName: false,
        //to show the ingredients value or not
        showIngredients: false,
        //to show the calorie diet info at the bottom of the label
        showCalorieDiet: false,
        //to show the customizable footer which can contain html and js codes
        showCustomFooter: false,

        //to show the disclaimer text or not
        showDisclaimer: false,
        //the height in px of the disclaimer div
        scrollDisclaimerHeightComparison: 100,
        scrollDisclaimer: 95,
        valueDisclaimer: 'Please note that these nutrition values are estimated based on our standard serving portions. ' +
            'As food servings may have a slight variance each time you visit, please expect these values to be with in 10% +/- of your actual meal. ',
        ingredientLabel: 'INGREDIENTS:',
        valueCustomFooter: '',

        //the are to set some values as 'not applicable'. this means that the nutrition label will appear but the value will be a 'gray dash'
        naCalories: false,
        naFatCalories: false,
        naTotalFat: false,
        naSatFat: false,
        naTransFat: false,
        naPolyFat: false,
        naMonoFat: false,
        naCholesterol: false,
        naSodium: false,
        naPotassium: false, //this is for the legacy version
        naPotassium_2018: false, //this is for the 2018 version
        naTotalCarb: false,
        naFibers: false,
        naSugars: false,
        naAddedSugars: false,
        naSugarAlcohol: false,
        naProteins: false,
        naVitaminA: false,
        naVitaminC: false,
        naVitaminD: false,
        naCalcium: false,
        naIron: false,

        //these are the default values for the nutrition info
        valueServingWeightGrams: 0,
        valueServingPerContainer: nutriFields.serving_per_container.value,
        valueCalories: nutriFields.calories.value,
        valueFatCalories: 0,
        valueTotalFat: nutriFields.total_fat.value,
        valueSatFat: nutriFields.saturated_fat.value,
        valueTransFat: nutriFields.trans_fat.value,
        valuePolyFat: nutriFields.polyunsaturated_fat.value,
        valueMonoFat: nutriFields.monounsaturated_fat.value,
        valueCholesterol: nutriFields.cholesterol.value,
        valueSodium: nutriFields.sodium.value,
        valuePotassium: nutriFields.potassium.value, //this is for the legacy version
        valuePotassium_2018: nutriFields.potassium.value, //this is for the 2018 version
        valueTotalCarb: nutriFields.total_carbohydrates.value,
        valueFibers: nutriFields.dietary_fiber.value,
        valueSugars: nutriFields.sugars.value,
        valueAddedSugars: nutriFields.added_sugar.value,
        valueSugarAlcohol: nutriFields.sugar_alcohol.value,
        valueProteins: nutriFields.protein.value,
        valueVitaminA: 0,
        valueVitaminC: 0,
        valueVitaminD: nutriFields.vitamin_d.value,
        valueCalcium: nutriFields.calcium.value,
        valueIron: nutriFields.iron.value,

        //customizable units for the values
        unitCalories: '',
        unitFatCalories: '',
        unitTotalFat: '<span aria-hidden="true">g</span><span class="sr-only"> grams</span>',
        unitSatFat: '<span aria-hidden="true">g</span><span class="sr-only"> grams</span>',
        unitTransFat: '<span aria-hidden="true">g</span><span class="sr-only"> grams</span>',
        unitPolyFat: '<span aria-hidden="true">g</span><span class="sr-only"> grams</span>',
        unitMonoFat: '<span aria-hidden="true">g</span><span class="sr-only"> grams</span>',
        unitCholesterol: '<span aria-hidden="true">mg</span><span class="sr-only"> milligrams</span>',
        unitSodium: '<span aria-hidden="true">mg</span><span class="sr-only"> milligrams</span>',
        unitPotassium: '<span aria-hidden="true">mg</span><span class="sr-only"> milligrams</span>', //this is for the legacy version
        unitPotassium_base: '<span aria-hidden="true">mg</span><span class="sr-only"> milligrams</span>', //this is for the 2018 version
        unitPotassium_percent: '%', //this is for the 2018 version
        unitTotalCarb: '<span aria-hidden="true">g</span><span class="sr-only"> grams</span>',
        unitFibers: '<span aria-hidden="true">g</span><span class="sr-only"> grams</span>',
        unitSugars: '<span aria-hidden="true">g</span><span class="sr-only"> grams</span>',
        unitAddedSugars: '<span aria-hidden="true">g</span><span class="sr-only"> grams</span>',
        unitSugarAlcohol: '<span aria-hidden="true">g</span><span class="sr-only"> grams</span>',
        unitProteins: '<span aria-hidden="true">g</span><span class="sr-only"> grams</span>',
        unitVitaminA: '%',
        unitVitaminC: '%',
        unitVitaminD_base: '<span aria-hidden="true">mcg</span><span class="sr-only"> micrograms</span>', //this is for the 2018 version
        unitVitaminD_percent: '%', //this is for the 2018 version
        unitCalcium: '%',
        unitCalcium_base: '<span aria-hidden="true">mg</span><span class="sr-only"> milligrams</span>', //this is for the 2018 version
        unitCalcium_percent: '%', //this is for the 2018 version
        unitIron: '%',
        unitIron_base: '<span aria-hidden="true">mg</span><span class="sr-only"> milligrams</span>', //this is for the 2018 version
        unitIron_percent: '%', //this is for the 2018 version

        //these are the values for the optional calorie diet
        valueCol1CalorieDiet: 2000,
        valueCol2CalorieDiet: 2500,
        valueCol1DietaryTotalFat: 0,
        valueCol2DietaryTotalFat: 0,
        valueCol1DietarySatFat: 0,
        valueCol2DietarySatFat: 0,
        valueCol1DietaryCholesterol: 0,
        valueCol2DietaryCholesterol: 0,
        valueCol1DietarySodium: 0,
        valueCol2DietarySodium: 0,
        valueCol1DietaryPotassium: 0,
        valueCol2DietaryPotassium: 0,
        valueCol1DietaryTotalCarb: 0,
        valueCol2DietaryTotalCarb: 0,
        valueCol1Dietary: 0,
        valueCol2Dietary: 0,

        //these text settings is so you can create nutrition labels in different languages or to simply change them to your need
        textNutritionFacts: transFields.textNutritionFacts,
        textDailyValues: transFields.textDailyValues,
        textServingSize: transFields.textServingSize,
        textServingsPerContainer: transFields.textServingsPerContainer,
        textAmountPerServing: transFields.textAmountPerServing,
        textCalories: transFields.textCalories,
        textFatCalories: transFields.textFatCalories,
        textTotalFat: transFields.textTotalFat,
        textSatFat: transFields.textSatFat,
        textTransFat: transFields.textTransFat,
        textPolyFat: transFields.textPolyFat,
        textMonoFat: transFields.textMonoFat,
        textCholesterol: transFields.textCholesterol,
        textSodium: transFields.textSodium,
        textPotassium: transFields.textPotassium,
        textTotalCarb: transFields.textTotalCarb,
        textFibers: transFields.textFibers,
        textSugars: transFields.textSugars,
        textAddedSugars1: transFields.textAddedSugars1,
        textAddedSugars2: transFields.textAddedSugars2,
        textSugarAlcohol: transFields.textSugarAlcohol,
        textProteins: transFields.textProteins,
        textVitaminA: transFields.textVitaminA,
        textVitaminC: transFields.textVitaminC,
        textVitaminD: transFields.textVitaminD,
        textCalcium: transFields.textCalcium,
        textIron: transFields.textIron,
        textNotApplicable: transFields.textNotApplicable,
        ingredientList: transFields.ingredientList,
        textPercentDailyPart1: transFields.textPercentDailyPart1,
        textPercentDailyPart2: transFields.textPercentDailyPart2,
        textPercentDaily2018VersionPart1: transFields.textPercentDaily2018VersionPart1,
        textPercentDaily2018VersionPart2: transFields.textPercentDaily2018VersionPart2,
        textGoogleAnalyticsEventCategory: transFields.textGoogleAnalyticsEventCategory,
        textGoogleAnalyticsEventActionUpArrow: transFields.textGoogleAnalyticsEventActionUpArrow,
        textGoogleAnalyticsEventActionDownArrow: transFields.textGoogleAnalyticsEventActionDownArrow,
        textGoogleAnalyticsEventActionTextbox: transFields.textGoogleAnalyticsEventActionTextbox,

        showLegacyVersion: false,
        legacyVersion: 1
    });
});