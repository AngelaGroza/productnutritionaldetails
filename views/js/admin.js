/**
 * NOTICE OF LICENSE
 *
 * This file is licensed under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code.
 *
 * @author    Active Design <office@activedesign.ro>
 * @copyright 2016-2018 Active Design
 * @license   LICENSE.txt
 */

document.addEventListener("DOMContentLoaded", function () {
    'use strict';

    $('body').on('click', '#nutritional_details_submit', function () {
        var url = $('#nutritional_details_form').data('action');
        var data = $('#nutritional_details_form input, #nutritional_details_form select, #nutritional_details_form textarea').serialize();
        var id_product = typeof window.id_product !== 'undefined' ? window.id_product : $('#form_id_product').val();


        $(this).prop('disabled', true).addClass('disabled');

        $.ajax({
            'type': 'post',
            'url': url,
            'data': data + '&id_product=' + id_product + '&productsExtraForm=1&ajax=1',
            'dataType': 'json',
            'success': function (resp) {
                $(this).prop('disabled', false).removeClass('disabled');

                var content = '';

                resp.errors.forEach(function (error) { content += '<li>' + error + '</li>'; });

                if (resp.errors.length) {
                    return $.growl.error({ title: '', size: 'large', message: '<ul class="list-unstyled">' + content + '</ul>' });
                }

                return $.growl.notice({ title: '', size: 'large', message: trans_successfully_updated });
            }.bind(this)
        });

    });
});
