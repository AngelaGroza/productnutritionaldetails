{**
 * NOTICE OF LICENSE
 *
 * This file is licensed under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code.
 *
 * @author    Active Design <office@activedesign.ro>
 * @copyright 2016-2018 Active Design
 * @license   LICENSE.txt
 *
 *}

{if ! $productExists}
        <div class="alert alert-warning">
            <div class="alert-text">{l s='Product has not been saved yet.' mod='productnutritionaldetails'}</div>
        </div>
    {else}
    {if $legacyAdminPanel}<div class="panel product-tab">{/if}
        <div id="nutritional_details_form" data-action="{$moduleUrl|escape:'htmlall':'UTF-8'}">
            {foreach $nutriFieldGroups as $groupName => $group}
                <div id="nd_group_{$groupName|escape:'htmlall':'UTF-8'}">
                    {if $groupName != 'display'}<hr>{/if}
                    {foreach $group as $fields}
                        {if is_array($fields)}
                            <div class="row">
                                {foreach $fields as $field}
                                    {$fieldData = $nutriFields[$field]}
                                    <div class="{if $field == 'footnote'}col-md-8{else}col-md-4{/if}">
                                        {include file='./products_extra/field.tpl' field=$fieldData}
                                    </div>
                                {/foreach}
                            </div>
                        {else}
                            {$fieldData = $nutriFields[$fields]}
                            <div class="row">
                                <div class="col-md-12">
                                    {include file='./products_extra/field.tpl' field=$fieldData}
                                </div>
                            </div>
                        {/if}
                    {/foreach}
                </div>
            {/foreach}
            <hr>
            <div class="row">
                <div class="col-md-offset-4 col-md-4">
                    <button type="button" class="btn btn-lg btn-block btn-primary" id="nutritional_details_submit">
                        {l s='Save' mod='productnutritionaldetails'}
                    </button>
                </div>
            </div>
        </div>
    {if $legacyAdminPanel}</div>{/if}
{/if}