{**
 * NOTICE OF LICENSE
 *
 * This file is licensed under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code.
 *
 * @author    Active Design <office@activedesign.ro>
 * @copyright 2016-2018 Active Design
 * @license   LICENSE.txt
 *
 *}

<div id="nutriFacts"></div>
{literal}
<script>
    var nutriFields = {/literal}{$nutriFields nofilter}{* This is JSON Content *}{literal};
    var transFields = {/literal}{$transFields nofilter}{* This is JSON Content *}{literal};
</script>
{/literal}