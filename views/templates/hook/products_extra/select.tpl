{**
 * NOTICE OF LICENSE
 *
 * This file is licensed under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code.
 *
 * @author    Active Design <office@activedesign.ro>
 * @copyright 2016-2018 Active Design
 * @license   LICENSE.txt
 *
 *}

<label for="nd_{$name|escape:'htmlall':'UTF-8'}">{$label|escape:'htmlall':'UTF-8'}</label>
<select name="{$name|escape:'htmlall':'UTF-8'}" id="nd_{$name|escape:'htmlall':'UTF-8'}" class="form-control" data-toggle="select2">
    {foreach $options as $option}
        <option value="{$option.value|escape:'htmlall':'UTF-8'}"{if $value == $option.value} selected{/if}>{$option.label|escape:'htmlall':'UTF-8'}</option>
    {/foreach}
</select>