{**
 * NOTICE OF LICENSE
 *
 * This file is licensed under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code.
 *
 * @author    Active Design <office@activedesign.ro>
 * @copyright 2016-2018 Active Design
 * @license   LICENSE.txt
 *
 *}

<div class="form-group">
    <label for="nd_{$name|escape:'htmlall':'UTF-8'}">{$label|escape:'htmlall':'UTF-8'}</label>
    <textarea name="{$name|escape:'htmlall':'UTF-8'}" id="nd_{$name|escape:'htmlall':'UTF-8'}" rows="10" class="form-control">{$value|escape:'htmlall':'UTF-8'}</textarea>
</div>