{**
 * NOTICE OF LICENSE
 *
 * This file is licensed under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code.
 *
 * @author    Active Design <office@activedesign.ro>
 * @copyright 2016-2018 Active Design
 * @license   LICENSE.txt
 *
 *}

{if $field.type == 'float'}
    {include file='./number.tpl' name=$field.name label=$field.label value=$field.value unit=$field.unit}
{elseif $field.type == 'enum'}
    {include file='./select.tpl' name=$field.name label=$field.label value=$field.value options=$field.options}
{elseif $field.type == 'bool'}
    {include file='./checkbox.tpl' name=$field.name label=$field.label value=$field.value}
{elseif $field.type == 'text'}
    {include file='./text.tpl' name=$field.name label=$field.label value=$field.value}
{elseif $field.type == 'textarea'}
    {include file='./textarea.tpl' name=$field.name label=$field.label value=$field.value}
{/if}