{**
 * NOTICE OF LICENSE
 *
 * This file is licensed under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code.
 *
 * @author    Active Design <office@activedesign.ro>
 * @copyright 2016-2018 Active Design
 * @license   LICENSE.txt
 *
 *}

<div class="form-group">
    <label for="nd_{$name|escape:'htmlall':'UTF-8'}">{$label|escape:'htmlall':'UTF-8'}</label>
    {if $unit}
        <div class="input-group">
            <input type="number" step="0.01" min="0" class="form-control" id="nd_{$name|escape:'htmlall':'UTF-8'}" name="{$name|escape:'htmlall':'UTF-8'}" value="{$value|escape:'htmlall':'UTF-8'}">
            <div class="input-group-addon" data-original="{$unit|escape:'htmlall':'UTF-8'}">{$unit|escape:'htmlall':'UTF-8'}</div>
        </div>
    {else}
        <input type="number" step="0.01" min="0" class="form-control" id="nd_{$name|escape:'htmlall':'UTF-8'}" name="{$name|escape:'htmlall':'UTF-8'}" value="{$value|escape:'htmlall':'UTF-8'}">
    {/if}
</div>