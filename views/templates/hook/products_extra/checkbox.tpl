{**
 * NOTICE OF LICENSE
 *
 * This file is licensed under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code.
 *
 * @author    Active Design <office@activedesign.ro>
 * @copyright 2016-2018 Active Design
 * @license   LICENSE.txt
 *
 *}

<label for="nd_{$name|escape:'htmlall':'UTF-8'}">
    <input type="checkbox" id="nd_{$name|escape:'htmlall':'UTF-8'}" name="{$name|escape:'htmlall':'UTF-8'}"{if $value} checked{/if}> {$label}
</label>