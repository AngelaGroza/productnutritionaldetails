{**
 * NOTICE OF LICENSE
 *
 * This file is licensed under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code.
 *
 * @author    Active Design <office@activedesign.ro>
 * @copyright 2016-2018 Active Design
 * @license   LICENSE.txt
 *
 *}

{if !empty($errors)}
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert">×</button>
        {if count($errors) == 1}
            {$errors.0|escape:'htmlall':'UTF-8'}
        {else}
            <ul>
                {foreach $errors as $error}
                    <li>{$error|escape:'htmlall':'UTF-8'}</li>
                {/foreach}
            </ul>
        {/if}
    </div>
{/if}

{if !empty($confirmations)}
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert">×</button>
        {if count($confirmations) == 1}
            {$confirmations.0|escape:'htmlall':'UTF-8'}
        {else}
            <ul>
                {foreach $confirmations as $confirmation}
                    <li>{$confirmation|escape:'htmlall':'UTF-8'}</li>
                {/foreach}
            </ul>
        {/if}
    </div>
{/if}

{$moduleForm}{* This is HTML content *}