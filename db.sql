CREATE TABLE `PREFIX_pnd_product_facts` (
    `id_facts` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `id_product` int(11) unsigned NOT NULL,

    `display` int(1) unsigned NOT NULL DEFAULT 1,

    `serving_per_container` decimal(20,6) unsigned NOT NULL DEFAULT 0,
    `units` varchar(255) DEFAULT NULL,
    `size` decimal(20,6) unsigned NOT NULL DEFAULT 0,
    `calories` decimal(20,6) unsigned NOT NULL DEFAULT 0,

    `total_fat` decimal(20,6) unsigned NOT NULL DEFAULT 0,
    `saturated_fat` decimal(20,6) unsigned NOT NULL DEFAULT 0,
    `trans_fat` decimal(20,6) unsigned NOT NULL DEFAULT 0,
    `polyunsaturated_fat` decimal(20,6) unsigned NOT NULL DEFAULT 0,
    `monounsaturated_fat` decimal(20,6) unsigned NOT NULL DEFAULT 0,
    `cholesterol` decimal(20,6) unsigned NOT NULL DEFAULT 0,
    `sodium` decimal(20,6) unsigned NOT NULL DEFAULT 0,
    `total_carbohydrates` decimal(20,6) unsigned NOT NULL DEFAULT 0,
    `dietary_fiber` decimal(20,6) unsigned NOT NULL DEFAULT 0,
    `sugars` decimal(20,6) unsigned NOT NULL DEFAULT 0,
    `added_sugar` decimal(20,6) unsigned NOT NULL DEFAULT 0,
    `sugar_alcohol` decimal(20,6) unsigned NOT NULL DEFAULT 0,
    `protein` decimal(20,6) unsigned NOT NULL DEFAULT 0,

    `calcium` decimal(20,6) unsigned NOT NULL DEFAULT 0,
    `iron` decimal(20,6) unsigned NOT NULL DEFAULT 0,
    `vitamin_d` decimal(20,6) unsigned NOT NULL DEFAULT 0,
    `potassium` decimal(20,6) unsigned NOT NULL DEFAULT 0,

    PRIMARY KEY (`id_facts`)
) ENGINE=ENGINE_TYPE DEFAULT CHARSET=utf8;