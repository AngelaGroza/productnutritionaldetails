<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitb151890fb9f044d6e6d833c385392dcd
{
    public static $prefixLengthsPsr4 = array (
        'P' => 
        array (
            'ProductNutritionalDetails\\App\\' => 30,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'ProductNutritionalDetails\\App\\' => 
        array (
            0 => __DIR__ . '/../..' . '/app',
        ),
    );

    public static $classMap = array (
        'ProductNutritionalDetails\\App\\Forms\\AdminProductsExtraForm' => __DIR__ . '/../..' . '/app/forms/AdminProductsExtraForm.php',
        'ProductNutritionalDetails\\App\\Forms\\Form' => __DIR__ . '/../..' . '/app/forms/Form.php',
        'ProductNutritionalDetails\\App\\Models\\Model' => __DIR__ . '/../..' . '/app/models/Model.php',
        'ProductNutritionalDetails\\App\\Models\\NutritionalFacts' => __DIR__ . '/../..' . '/app/models/NutritionalFacts.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitb151890fb9f044d6e6d833c385392dcd::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitb151890fb9f044d6e6d833c385392dcd::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInitb151890fb9f044d6e6d833c385392dcd::$classMap;

        }, null, ClassLoader::class);
    }
}
