<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licensed under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code.
 *
 * @author    Active Design <office@activedesign.ro>
 * @copyright 2016-2018 Active Design
 * @license   LICENSE.txt
 */

if (! defined('_PS_VERSION_')) {
    exit;
}

require_once _PS_MODULE_DIR_.'productnutritionaldetails/vendor/autoload.php';

class Productnutritionaldetails extends Module
{
    /**
     * Error messages bag.
     *
     * @var array
     */
    protected $errors = array();

    /**
     * Confirmation messages bag.
     *
     * @var array
     */
    protected $confirmations = array();

    /**
     * Module required hooks.
     *
     * @var array
     */
    protected $hooks = array(
        'header',
        'displayBackOfficeHeader',
        array('version' => '1.7', 'name' => 'displayReassurance'),
        array('version' => '1.6', 'name' => 'displayRightColumnProduct'),
        'displayRightColumnProduct',
        'displayAdminProductsExtra',
    );

    /**
     * General configuration.
     *
     * @var array
     */
    protected $configs = array('PRND_ENABLED' => true);

    /**
     * Module database tables
     *
     * @var array
     */
    protected static $tables = array('pnd_product_facts');

    /**
     * Nutritional details form groups.
     *
     * @var array
     */
    protected static $productFormGroups = array(
        'display' => array(
            'display',
        ),

        'serving' => array(
            array('serving_per_container'),
            array('units', 'size'),
        ),

        'calories' => array(array('calories')),

        'fat' => array(
            array('total_fat', 'saturated_fat', 'trans_fat'),
            array('polyunsaturated_fat', 'monounsaturated_fat', 'cholesterol'),
            array('sodium', 'total_carbohydrates', 'dietary_fiber'),
            array('sugars', 'added_sugar', 'sugar_alcohol'),
            array('protein'),
        ),

        'vitamins' => array(
            array('calcium', 'iron'),
            array('vitamin_d', 'potassium'),
        ),
    );
    
    /**
     * Create new module instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->name = 'productnutritionaldetails';
        $this->tab = 'front_office_features';
        $this->version = '1.0.1';
        $this->author = 'Active Design';
        $this->need_instance = 0;
        $this->bootstrap = true;
        $this->module_key = '02b3b7b509e4abc0750ad18b643e5baf';
    
        $this->author_address = '0xc0D7cE57752e47305707d7174B9686C0Afb229c3';
        
        parent::__construct();
        
        $this->displayName = $this->l('Product Nutritional Details Pro');
        $this->description = $this->l('Display nutrition facts on your product pages: energy, fat, carbohydrate, protein, etc.');
        $this->confirmUninstall = $this->l('Are you sure you want to uninstall this module?');
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    /**
     * Install the module.
     *
     * @return bool
     */
    public function install()
    {
        return parent::install() &&
            $this->migrateDb() &&
            $this->registerHooks() &&
            $this->createConfigs();
    }

    /**
     * Un-install the module.
     *
     * @return bool
     */
    public function uninstall()
    {
        return parent::uninstall() &&
            $this->dropDb() &&
            $this->unregisterHooks() &&
            $this->deleteConfigs();
    }

    /**
     * Register the module hooks.
     *
     * @return bool
     */
    protected function registerHooks()
    {
        $shopVersion = Tools::substr(_PS_VERSION_, 0, 3);

        $hooks = array_filter($this->hooks, function ($hook) use ($shopVersion) {
            return is_string($hook) || (is_array($hook) && $hook['version'] === $shopVersion);
        });

        foreach ($hooks as $hook) {
            if (! $this->registerHook(is_array($hook) ? $hook['name'] : $hook)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Create the module configuration.
     *
     * @return bool
     */
    protected function createConfigs()
    {
        foreach ($this->getConfigs() as $config => $value) {
            if (! Configuration::updateValue($config, $value)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Unregister the module hooks.
     *
     * @return bool
     */
    protected function unregisterHooks()
    {
        $shopVersion = Tools::substr(_PS_VERSION_, 0, 3);

        $hooks = array_filter($this->hooks, function ($hook) use ($shopVersion) {
            return is_string($hook) || (is_array($hook) && $hook['version'] === $shopVersion);
        });

        foreach ($hooks as $hook) {
            if (! $this->unregisterHook(is_array($hook) ? $hook['name'] : $hook)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Delete the module configuration.
     *
     * @return bool
     */
    protected function deleteConfigs()
    {
        foreach (array_keys($this->getConfigs()) as $config) {
            if (! Configuration::deleteByName($config)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Migrate the required database tables
     *
     * @return bool
     */
    protected function migrateDb()
    {
        $sql_file = $this->getLocalPath().'db.sql';

        if (! file_exists($sql_file) ||
            ! $sql = Tools::file_get_contents($sql_file)) {
            return false;
        }
        
        $sql = preg_split(
            '/;\s*[\r\n]+/',
            str_replace(
                array('PREFIX_', 'ENGINE_TYPE'),
                array(_DB_PREFIX_, _MYSQL_ENGINE_),
                $sql
            )
        );
        
        foreach ($sql as $query) {
            if ($query) {
                if (! Db::getInstance()->execute(trim($query))) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Drop the database tables
     *
     * @return bool
     */
    protected function dropDb()
    {
        foreach (static::$tables as $table) {
            if (! Db::getInstance()->execute('drop table if exists '._DB_PREFIX_.$table)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Get module configuration page content.
     *
     * @return string
     */
    public function getContent()
    {
        $this->postProcess();

        $settingsForm = $this
            ->formFactory(array('fields_value' => $this->getSettingsFormValues()))
            ->generateForm(array(array('form' => $this->getSettingsForm())));

        return $this->context->smarty
            ->assign(array(
                'errors' => $this->errors,
                'confirmations' => $this->confirmations,
                'moduleForm' => $settingsForm,
            ))->fetch($this->getLocalPath().'views/templates/admin/master.tpl');
    }

    /**
     * Process any post request.
     *
     * @return void
     */
    public function postProcess()
    {
        if (Tools::isSubmit('settingsForm')) {
            $this->processSettingsForm();
        } elseif (Tools::isSubmit('productsExtraForm')) {
            $this->processProductsExtraForm();
        }
    }

    protected function processSettingsForm()
    {
        foreach ($this->getConfigKeys() as $key) {
            $value = Tools::getValue($key);

            if (! Configuration::updateValue($key, $value)) {
                $this->errors[] = $this->l('Something went wrong, please try again.');
                return ;
            }
        }

        $this->confirmations[] = $this->l('Settings updated.');
    }

    protected function processProductsExtraForm()
    {
        $results = (new \ProductNutritionalDetails\App\Forms\AdminProductsExtraForm())->save()->getResults();

        header('Content-type:application/json;charset=utf-8');
        die(json_encode($results));
    }

    protected function getSettingsForm()
    {
        return array(
            'legend' => array('title' => $this->l('General settings'), 'icon' => 'icon-cog'),
            'description' => $this->l('Edit product nutritional details from').': '.$this->getDirections().'.',
            'input' => array(
                array(
                    'type' => 'switch',
                    'label' => $this->l('Enable product nutritional details'),
                    'name' => 'PRND_ENABLED',
                    'class' => 'fixed-width-xs',
                    'values' => $this->getYesOrNoValues(),
                    'col' => 2,
                ),
            ),
            'submit' => array('title' => $this->l('Save'), 'name' => 'settingsForm'),
        );
    }

    protected function getDirections()
    {
        if (Tools::version_compare(_PS_VERSION_, '1.7', '<')) {
            return $this->l('Catalog > Products > Edit Product').' > '. $this->displayName;
        }

        return $this->l('Catalog > Products > Edit Product > Modules').' > '. $this->displayName;
    }

    protected function getSettingsFormValues()
    {
        $values = array();

        foreach ($this->getConfigKeys() as $key) {
            $values[$key] = Configuration::get($key);
        }

        return $values;
    }

    /**
     * Generate helper form object based on the fields.
     *
     * @param  array $fields
     * @return \HelperForm
     */
    protected function formFactory($fields = array())
    {
        $helper = new HelperForm;

        // General settings
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->currentIndex = $this->getIndex(false);
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        // Additional settings if any
        foreach ($fields as $name => $value) {
            $helper->$name = $value;
        }

        if (! array_key_exists('languages', $helper->tpl_vars)) {
            $helper->tpl_vars['languages'] = $this->context->controller->getLanguages();
        }

        if (! array_key_exists('id_language', $helper->tpl_vars)) {
            $helper->tpl_vars['id_language'] = $this->context->language->id;
        }

        return $helper;
    }

    /**
     * Get the module admin index URL.
     *
     * @param  bool $withToken
     * @return string
     */
    protected function getIndex($withToken = true)
    {
        return $this->context->link->getAdminLink('AdminModules', $withToken)
            .'&configure='.$this->name
            .'&tab_module='.$this->tab
            .'&module_name='.$this->name;
    }

    /**
     * Get repetitive switch values
     *
     * @return array
     */
    protected function getYesOrNoValues()
    {
        static $switchValues;

        return $switchValues ?:
            $switchValues = array(
                array('id' => 'active_on', 'value' => 1, 'label' => $this->l('Yes')),
                array('id' => 'active_off', 'value' => 0, 'label' => $this->l('No')),
            );
    }

    /**
     * Get all module configs.
     *
     * @return array
     */
    protected function getConfigs()
    {
        return $this->configs;
    }

    /**
     * Get all module config keys.
     *
     * @return array
     */
    protected function getConfigKeys()
    {
        return array_keys($this->configs);
    }

    /**
     * Display module settings for the given product in the back-office.
     *
     * @param  array $params
     * @return string
     */
    public function hookDisplayAdminProductsExtra($params)
    {
        $productExists = Tools::version_compare(_PS_VERSION_, '1.7', '>=') || (bool)Tools::getValue('id_product') !== false;

        if (! $productExists) {
            return $this->context->smarty
                ->assign(array(
                    'productExists' => $productExists,
                ))
                ->fetch($this->getLocalPath().'views/templates/hook/products_extra.tpl');
        }

        $productId = Tools::version_compare(_PS_VERSION_, '1.7', '>=') ? (int)$params['id_product'] : (int)Tools::getValue('id_product');

        $nutriFacts = \ProductNutritionalDetails\App\Models\NutritionalFacts::findByProduct($productId);

        $fields = $nutriFacts->getSortedFields();

        return $this->context->smarty
            ->assign(array(
                'productExists' => $productExists,
                'legacyAdminPanel' => ! Tools::version_compare(_PS_VERSION_, '1.7', '>='),
                'moduleUrl' => $this->getIndex(),
                'nutriFields' => $this->getProductFormFields($fields),
                'nutriFieldGroups' => static::$productFormGroups,
            ))
            ->fetch($this->getLocalPath().'views/templates/hook/products_extra.tpl');
    }

    protected function getProductFormFields($fields)
    {
        $this->addFieldLabels($fields);

        return $this->getFieldWithMappedKeys($fields);
    }

    protected function addFieldLabels(&$fields)
    {
        array_walk($fields, function (&$field) {
            switch ($field['name']) {
                case 'display':
                    $field['label'] = $this->l('Display nutrition facts label');
                    break;
                case 'serving_per_container':
                    $field['label'] = $this->l('Serving per container');
                    break;
                case 'units':
                    $field['label'] = $this->l('Units');
                    break;
                case 'size':
                    $field['label'] = $this->l('Size');
                    break;
                case 'calories':
                    $field['label'] = $this->l('Calories');
                    break;
                case 'total_fat':
                    $field['label'] = $this->l('Total fat');
                    break;
                case 'saturated_fat':
                    $field['label'] = $this->l('Saturated fat');
                    break;
                case 'trans_fat':
                    $field['label'] = $this->l('Trans fat');
                    break;
                case 'polyunsaturated_fat':
                    $field['label'] = $this->l('Polyunsaturated fat');
                    break;
                case 'monounsaturated_fat':
                    $field['label'] = $this->l('Monounsaturated fat');
                    break;
                case 'cholesterol':
                    $field['label'] = $this->l('Cholesterol');
                    break;
                case 'sodium':
                    $field['label'] = $this->l('Sodium');
                    break;
                case 'total_carbohydrates':
                    $field['label'] = $this->l('Total carbohydrates');
                    break;
                case 'dietary_fiber':
                    $field['label'] = $this->l('Dietary fiber');
                    break;
                case 'sugars':
                    $field['label'] = $this->l('Sugars');
                    break;
                case 'added_sugar':
                    $field['label'] = $this->l('Added sugar');
                    break;
                case 'sugar_alcohol':
                    $field['label'] = $this->l('Sugar alcohol');
                    break;
                case 'protein':
                    $field['label'] = $this->l('Protein');
                    break;
                case 'calcium':
                    $field['label'] = $this->l('Calcium');
                    break;
                case 'iron':
                    $field['label'] = $this->l('Iron');
                    break;
                case 'vitamin_d':
                    $field['label'] = $this->l('Vitamin D');
                    break;
                case 'potassium':
                    $field['label'] = $this->l('Potassium');
                    break;
            }
        });
    }

    protected function getFieldWithMappedKeys($fields)
    {
        $fieldKeys = array_map(function ($field) {
            return $field['name'];
        }, $fields);

        return array_combine($fieldKeys, $fields);
    }

    public function hookDisplayBackOfficeHeader($params)
    {
        $this->context->controller->addCSS($this->getPathUri().'views/css/admin.css');
        $this->context->controller->addJS($this->getPathUri().'views/js/admin.js');

        Media::addJsDef(array('trans_successfully_updated' => $this->l('Successfully updated.')));
    }

    public function hookHeader($params)
    {
        if (! $this->context->controller instanceof ProductController) {
            return ;
        }

        $this->context->controller->addCSS($this->getPathUri().'views/css/nutritionLabel.css');
        $this->context->controller->addJS($this->getPathUri().'views/js/nutritionLabel.js');
        $this->context->controller->addJS($this->getPathUri().'views/js/front.js');
    }

    public function hookDisplayReassurance($params)
    {
        if (! (bool)Configuration::get('PRND_ENABLED')) {
            return null;
        }

        $productId = (int)Tools::getValue('id_product');

        $nutriFacts = \ProductNutritionalDetails\App\Models\NutritionalFacts::findByProduct($productId);

        if (! Validate::isLoadedObject($nutriFacts) || ! (bool)$nutriFacts->display) {
            return null;
        }

        $fields = $nutriFacts->getSortedFields();

        return $this->context->smarty
            ->assign(array(
                'nutriFields' => json_encode($this->getProductFormFields($fields)),
                'transFields' => json_encode($this->getTranslatableFields()),
            ))
            ->fetch($this->getLocalPath().'views/templates/hook/product_page.tpl');
    }

    public function hookDisplayRightColumnProduct($params)
    {
        return $this->hookDisplayReassurance($params);
    }

    protected function getTranslatableFields()
    {
        return array(
            'textNutritionFacts' => $this->l('Nutrition Facts'),
            'textDailyValues' => $this->l('Daily Value'),
            'textServingSize' => $this->l('Serving Size:'),
            'textServingsPerContainer' => $this->l('Servings Per Container'),
            'textAmountPerServing' => $this->l('Amount Per Serving'),
            'textCalories' => $this->l('Calories'),
            'textFatCalories' => $this->l('Calories from Fat'),
            'textTotalFat' => $this->l('Total Fat'),
            'textSatFat' => $this->l('Saturated Fat'),
            'textTransFat' => sprintf($this->l('%sTrans%s Fat'), '<em>', '</em>'),
            'textPolyFat' => $this->l('Polyunsaturated Fat'),
            'textMonoFat' => $this->l('Monounsaturated Fat'),
            'textCholesterol' => $this->l('Cholesterol'),
            'textSodium' => $this->l('Sodium'),
            'textPotassium' => $this->l('Potassium'),
            'textTotalCarb' => $this->l('Total Carbohydrates'),
            'textFibers' => $this->l('Dietary Fiber'),
            'textSugars' => $this->l('Sugars'),
            'textAddedSugars1' => $this->l('Includes '),
            'textAddedSugars2' => $this->l(' Added Sugars'),
            'textSugarAlcohol' => $this->l('Sugar Alcohol'),
            'textProteins' => $this->l('Protein'),
            'textVitaminA' => $this->l('Vitamin A'),
            'textVitaminC' => $this->l('Vitamin C'),
            'textVitaminD' => $this->l('Vitamin D'),
            'textCalcium' => $this->l('Calcium'),
            'textIron' => $this->l('Iron'),
            'textNotApplicable' => $this->l('-'),
            'ingredientList' => $this->l('None'),
            'textPercentDailyPart1' => $this->l('Percent Daily Values are based on a'),
            'textPercentDailyPart2' => $this->l('calorie diet'),
            'textPercentDaily2018VersionPart1' => $this->l('The % Daily Value (DV) tells you how much a nutrient in a serving of food contributes to a daily diet. '),
            'textPercentDaily2018VersionPart2' => $this->l(' calories a day is used for general nutrition advice.'),
            'textGoogleAnalyticsEventCategory' => $this->l('Nutrition Label'),
            'textGoogleAnalyticsEventActionUpArrow' => $this->l('Quantity Up Arrow Clicked'),
            'textGoogleAnalyticsEventActionDownArrow' => $this->l('Quantity Down Arrow Clicked'),
            'textGoogleAnalyticsEventActionTextbox' => $this->l('Quantity Textbox Changed'),
        );
    }
}
