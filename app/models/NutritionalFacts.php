<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licensed under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code.
 *
 * @author    Active Design <office@activedesign.ro>
 * @copyright 2016-2018 Active Design
 * @license   LICENSE.txt
 */

namespace ProductNutritionalDetails\App\Models;

use Db;

class NutritionalFacts extends Model
{
    public $id_facts;
    public $id_product;

    public $display = true;

    public $serving_per_container = 0;
    public $units = null;
    public $size = 0;
    public $calories = 0;

    public $total_fat = 0;
    public $saturated_fat = 0;
    public $trans_fat = 0;
    public $polyunsaturated_fat = 0;
    public $monounsaturated_fat = 0;
    public $cholesterol = 0;
    public $sodium = 0;
    public $total_carbohydrates = 0;
    public $dietary_fiber = 0;
    public $sugars = 0;
    public $added_sugar = 0;
    public $sugar_alcohol = 0;
    public $protein = 0;

    public $calcium = 0;
    public $iron = 0;
    public $vitamin_d = 0;
    public $potassium = 0;

    const INT_RULES = array('type' => self::TYPE_FLOAT, 'validate' => 'isUnsignedFloat');

    const BOOL_RULES = array('type' => self::TYPE_BOOL, 'validate' => 'isBool');

    public static $definition = array(
        'table' => 'pnd_product_facts',
        'primary' => 'id_facts',
        'fields' => array(
            'id_product' => self::INT_RULES,

            'display' => self::BOOL_RULES,

            'serving_per_container' => self::INT_RULES,
            'units' => array(
                'type' => self::TYPE_NOTHING,
                'validate' => 'isName',
            ),

            'size' => self::INT_RULES,
            'calories' => self::INT_RULES,

            'total_fat' => self::INT_RULES,
            'saturated_fat' => self::INT_RULES,
            'trans_fat' => self::INT_RULES,
            'polyunsaturated_fat' => self::INT_RULES,
            'monounsaturated_fat' => self::INT_RULES,
            'cholesterol' => self::INT_RULES,
            'sodium' => self::INT_RULES,
            'total_carbohydrates' => self::INT_RULES,
            'dietary_fiber' => self::INT_RULES,
            'sugars' => self::INT_RULES,
            'added_sugar' => self::INT_RULES,
            'sugar_alcohol' => self::INT_RULES,
            'protein' => self::INT_RULES,

            'calcium' => self::INT_RULES,
            'iron' => self::INT_RULES,
            'vitamin_d' => self::INT_RULES,
            'potassium' => self::INT_RULES,
        ),
    );

    protected static $fieldUnits = array(
        'display' => null,

        'serving_per_container' => null,
        'units' => null,
        'size' => null,
        'calories' => null,

        'total_fat' => 'g',
        'saturated_fat' => 'g',
        'trans_fat' => 'g',
        'polyunsaturated_fat' => 'g',
        'monounsaturated_fat' => 'g',
        'cholesterol' => 'g',
        'sodium' => 'g',
        'total_carbohydrates' => 'g',
        'dietary_fiber' => 'g',
        'sugars' => 'g',
        'added_sugar' => 'g',
        'sugar_alcohol' => 'g',
        'protein' => 'g',

        'calcium' => '%',
        'iron' => '%',
        'vitamin_d' => '%',
        'potassium' => '%',
    );

    public function setFields($fields)
    {
        foreach ($fields as $field => $value) {
            $this->$field = $value;
        }

        return $this;
    }

    public function getSortedFields()
    {
        $groups = array();

        foreach (static::$definition['fields'] as $field => $rules) {
            if ($field == 'id_product') {
                continue;
            }

            if ($rules['type'] === self::TYPE_FLOAT && $rules['validate'] === 'isUnsignedFloat') {
                $groups[] = array('type' => 'float', 'name' => $field, 'value' => (float)$this->$field, 'unit' => static::$fieldUnits[$field]);
            } elseif ($rules['type'] === self::TYPE_BOOL) {
                $groups[] = array('type' => 'bool', 'name' => $field, 'value' => (bool)$this->$field, 'unit' => static::$fieldUnits[$field]);
            } elseif ($rules['type'] === self::TYPE_STRING && isset($rules['values'])) {
                $groups[] = array('type' => 'enum', 'name' => $field, 'value' => $this->$field, 'options' => $rules['values'], 'unit' => static::$fieldUnits[$field]);
            } elseif ($rules['type'] === self::TYPE_NOTHING && $rules['validate'] === 'isName') {
                $groups[] = array('type' => 'text', 'name' => $field, 'value' => $this->$field, 'unit' => static::$fieldUnits[$field]);
            } elseif ($rules['type'] === self::TYPE_NOTHING && $rules['validate'] === 'isMessage') {
                $groups[] = array('type' => 'textarea', 'name' => $field, 'value' => $this->$field, 'unit' => static::$fieldUnits[$field]);
            }
        }

        return $groups;
    }

    public static function findByProduct($productId)
    {
        $idFacts = (int)Db::getInstance()->getValue(
            'select `id_facts` from `'._DB_PREFIX_.static::$definition['table'].'` where `id_product` = '. (int)$productId
        );

        if (! $idFacts) {
            return new static;
        }

        return new static($idFacts);
    }
}
