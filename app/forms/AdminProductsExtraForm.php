<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licensed under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code.
 *
 * @author    Active Design <office@activedesign.ro>
 * @copyright 2016-2018 Active Design
 * @license   LICENSE.txt
 */

namespace ProductNutritionalDetails\App\Forms;

use Product;
use Validate;
use ProductNutritionalDetails\App\Models\NutritionalFacts;

class AdminProductsExtraForm extends Form
{
    public $id_product;

    public $display;
    public $serving_per_container;
    public $units;
    public $size;
    public $calories;

    public $total_fat;
    public $saturated_fat;
    public $trans_fat;
    public $polyunsaturated_fat;
    public $monounsaturated_fat;
    public $cholesterol;
    public $sodium;
    public $total_carbohydrates;
    public $dietary_fiber;
    public $sugars;
    public $added_sugar;
    public $sugar_alcohol;
    public $protein;

    public $calcium;
    public $iron;
    public $vitamin_d;
    public $potassium;


    /**
     * {@inheritdoc}
     *
     * @var array
     */
    protected $casts = array(
        'id_product' => 'int',
        'display' => 'bool',
        'calories' => 'float',
        'total_fat' => 'float',
        'saturated_fat' => 'float',
        'trans_fat' => 'float',
        'polyunsaturated_fat' => 'float',
        'monounsaturated_fat' => 'float',
        'cholesterol' => 'float',
        'sodium' => 'float',
        'total_carbohydrates' => 'float',
        'dietary_fiber' => 'float',
        'sugars' => 'float',
        'added_sugar' => 'float',
        'sugar_alcohol' => 'float',
        'protein' => 'float',
        'calcium' => 'float',
        'iron' => 'float',
        'vitamin_d' => 'float',
        'potassium' => 'float',
    );

    /**
     * {@inheritdoc}
     *
     * @var array
     */
    protected $sanitizers = array(
        'serving_per_container' => 'trim',
        'units' => 'trim',
        'size' => 'trim',
    );

    /**
     * {@inheritdoc}
     *
     * @param array $fields
     */
    public function __construct($fields = array())
    {
        parent::__construct($fields);

        foreach ($this->casts as $field => $cast) {
            if ($cast !== 'int' || $cast !== 'float') {
                continue;
            }

            $this->$field = $this->$field < 0 ? 0 : $this->$field;
        }

        foreach (array_keys($this->sanitizers) as $field) {
            if ($this->$field === '') {
                $this->$field = null;
            } elseif ($field != 'units') {
                $this->$field = (int)$this->$field < 0 ? 0 : (int)$this->$field;
            }
        }
    }

    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function validate()
    {
        // Validate product.
        if (! Product::existsInDatabase($this->id_product, Product::$definition['table'])) {
            $this->addError('Invalid product.');
        }

        return count($this->errors) == 0;
    }

    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    protected function persist()
    {
        $nutritionalFacts = NutritionalFacts::findByProduct($this->id_product);

        $nutritionalFacts->id_product = $this->id_product;

        $nutritionalFacts->display = $this->display;
        $nutritionalFacts->serving_per_container = $this->serving_per_container;
        $nutritionalFacts->units = $this->units;
        $nutritionalFacts->size = $this->size;
        $nutritionalFacts->calories = $this->calories;

        $nutritionalFacts->total_fat = $this->total_fat;
        $nutritionalFacts->saturated_fat = $this->saturated_fat;
        $nutritionalFacts->trans_fat = $this->trans_fat;
        $nutritionalFacts->polyunsaturated_fat = $this->polyunsaturated_fat;
        $nutritionalFacts->monounsaturated_fat = $this->monounsaturated_fat;
        $nutritionalFacts->cholesterol = $this->cholesterol;
        $nutritionalFacts->sodium = $this->sodium;
        $nutritionalFacts->total_carbohydrates = $this->total_carbohydrates;
        $nutritionalFacts->dietary_fiber = $this->dietary_fiber;
        $nutritionalFacts->sugars = $this->sugars;
        $nutritionalFacts->added_sugar = $this->added_sugar;
        $nutritionalFacts->sugar_alcohol = $this->sugar_alcohol;
        $nutritionalFacts->protein = $this->protein;

        $nutritionalFacts->calcium = $this->calcium;
        $nutritionalFacts->iron = $this->iron;
        $nutritionalFacts->vitamin_d = $this->vitamin_d;
        $nutritionalFacts->potassium = $this->potassium;

        if (! $nutritionalFacts->save(true)) {
            $this->addError('Something went wrong, please try again.');
            return false;
        }

        return true;
    }
}
