<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licensed under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code.
 *
 * @author    Active Design <office@activedesign.ro>
 * @copyright 2016-2018 Active Design
 * @license   LICENSE.txt
 */

namespace ProductNutritionalDetails\App\Forms;

use Tools;
use ReflectionClass;
use ReflectionProperty;

abstract class Form
{
    /**
     * Cast definition
     *
     * @var array
     */
    protected $casts = array();

    /**
     * Sanitizers definition
     *
     * @var array
     */
    protected $sanitizers = array();

    /**
     * Form field names
     *
     * @var array
     */
    protected $fields = array();

    /**
     * Validation errors
     *
     * @var array
     */
    protected $errors = array();

    /**
     * Create a new form instance
     *
     * @param array $fields
     * @return void
     */
    public function __construct($fields = array())
    {
        $this->fields = $fields;

        if (empty($this->fields)) {
            $this->setFields();
        }

        foreach ($this->fields as $field) {
            $this->castField($field);
            $this->sanitizeField($field);
        }
    }

    /**
     * Dynamically set the fields by retrieving
     * the public class field names using Reflection
     *
     * @return void
     */
    protected function setFields()
    {
        $props = (new ReflectionClass($this))->getProperties(
            ReflectionProperty::IS_PUBLIC
        );

        $this->fields = array_map(
            function ($prop) {
                return $prop->getName();
            },
            $props
        );
    }

    /**
     * Attempt to save the form data
     *
     * @return $this
     */
    public function save()
    {
        if ($this->validate()) {
            $this->persist();
        }

        return $this;
    }

    /**
     * Validate the form, populate errors array if needed
     *
     * @return bool
     */
    abstract public function validate();

    /**
     * Actually save the form data
     *
     * @return bool
     */
    abstract protected function persist();

    /**
     * Add a new error message
     *
     * @param string|array
     * @return void
     */
    protected function addError($error)
    {
        if (is_array($error)) {
            array_merge($this->errors, $error);
        } else {
            $this->errors[] = $error;
        }
    }

    /**
     * Get validation errors
     *
     * @return array
     */
    public function errors()
    {
        return $this->errors;
    }

    /**
     * Get form field names
     *
     * @return array
     */
    public function fields()
    {
        return $this->fields;
    }

    /**
     * Cast field values to their specified type
     *
     * @param  string
     * @return void
     */
    protected function castField($field)
    {
        if (! isset($this->casts[$field])) {
            $this->$field = Tools::getValue($field);
            return ;
        }

        if ($this->casts[$field] == 'int') {
            $this->$field = (int)Tools::getValue($field);
        } elseif ($this->casts[$field] == 'float') {
            $this->$field = (float)Tools::getValue($field);
        } elseif ($this->casts[$field] == 'bool') {
            $this->$field = static::castBool(Tools::getValue($field));
        } else {
            $this->$field = Tools::getValue($field);
        }
    }

    /**
     * Cast field values to specified type
     *
     * @param  mixed
     * @return bool
     */
    protected static function castBool($value)
    {
        if (is_string($value)) {
            // From checkboxes we get 'true' or 'on'
            if ($value == 'true' || $value == 'on') {
                return true;
            }

            return false;
        }

        return (bool)$value;
    }

    /**
     * Apply user functions to field values
     *
     * @param  string
     * @return void
     */
    protected function sanitizeField($field)
    {
        if (! isset($this->sanitizers[$field])) {
            return ;
        }

        foreach (explode('|', $this->sanitizers[$field]) as $func) {
            if (function_exists($func)) {
                $this->$field = $func($this->$field);
            }
        }
    }

    public function getResults()
    {
        return array('errors' => $this->errors);
    }
}
